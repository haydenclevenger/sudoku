# README #


### Description ###

This java repository implements a Sudoku puzzle solver.
Puzzles are 9x9 rows of numbers in .txt files.

### How do I get set up? ###

You will need to make your own unfinished puzzles for the program to solve.
Any number of them can be given to the program as command line arguments.
Solutions are created in the form of .txt files.

### Contribution guidelines ###

All contributions and suggestions welcome!

### Who do I talk to? ###

Hayden Clevenger
hayden.clev@gmail.com
# Simple makefile that builds all java classes in the current folder

JFLAGS = -g
JC = javac
SOURCES = $(wildcard *.java)

default: classes

classes: $(SOURCES:.java=.class)

%.class: %.java
	$(JC) $(JFLAGS) $<

clean:
	$(RM) *.class *~

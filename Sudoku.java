/******
 * Sudoku.java
 *
 * Compilation: 	javac Sudoku.java
 * Usage:			java Sudoku <filename_1>.txt <filename_2>.txt ... <filename_n>.txt
 *     where <filename_k> is a plaintext file that stores an unfinished Sudoku board, e.g.
 *
 *			1 0 6 0 0 0 9 0 0
 * 			0 2 0 6 0 0 4 0 3
 * 			4 8 7 0 0 5 1 0 0
 * 			0 0 5 4 0 3 6 0 0
 * 			3 0 0 0 5 0 0 0 4
 * 			0 0 4 7 0 6 8 0 0
 * 			0 0 1 3 0 0 2 7 6
 * 			7 0 3 0 0 2 0 4 0
 * 			0 0 2 0 0 0 3 0 9
 *
 *
 * Output: creates .txt files: <filename_1>_solution.txt ... <filename_n>_solution.txt that store the completed Sudoku
 * boards corresponding the input file(s).
 ******/

/*
* @author: Hayden Clevenger
* @email: haydenclevenger@sandiego.edu
* @date February 11, 2015
*/

import java.io.*;

public class Sudoku {

	public static void main (String[] args) {

		if (args.length < 1) {
			System.out.println("Missing Sudoku File");
			System.out.println("Usage:java Sudoku <filename_1>.txt <filename_2>.txt ... <filename_n>.txt");
			return;
		}


	//loop to account for multiple boards
		for(int loop = 0; loop < args.length; loop++) {

	
		//setting up a new board
			int[][] board = new int[9][9];
			board = boardSetup(board, args[loop]);
			int[][] origBoard = board;

	printBoard(board);

		//solving the board
			while(!boardIsSolved(board)) {
				board = boardUpdate(board);
			}

		//printing final board
			System.out.println("UNSOLVED BOARD: " + args[loop]);
			printBoard(origBoard);
			System.out.println("=================");
			System.out.println("SOLVED BOARD: " + args[loop]);
			printBoard(board);
			System.out.println("=================");
			System.out.println();

		//writing board to a file
			writeBoard(board, args[loop]);
		}
	}

/*
Method to setup the original Sudoku game board
@param board is the double array which will represent the board
@param table is the filename of the Sudoku table to read from
@return the filled in board
*/
	private static int[][] boardSetup(int[][] board, String table) {
	//setting up the reader
		File inputFile = null;
		BufferedReader reader = null;

		try {
		 	inputFile = new File(table);
			reader = new BufferedReader(new FileReader(inputFile));
		}

		catch (FileNotFoundException e) {
			System.out.println("File could not be found");
			System.exit(1);
		}

		catch(IOException e) {
			System.out.println("IOException");
			System.exit(1);
		}

		String firstLine = null;

	//looping though rows
		for(int i = 0; i < 9; i++) {
		//try catch block in case reader fails
			try {
				firstLine = reader.readLine();
			}

			catch (IOException e) {
				System.out.println("IOException in boardSetup");
				System.exit(1);
			}
			String[] intArray = firstLine.split(" ");
			
		//parsing the row into integers
			for(int j = 0; j < 9; j++) {
				board[i][j] = Integer.parseInt(intArray[j]);
			}
		}
		return board;
	}

/*
Method to update each board element
@param board is the sudoku board to work with 
@returns the updated board
*/
	private static int[][] boardUpdate(int[][] board) {
		for(int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {

			//creating array of possibilities
				int[] possibilities = spotCheck(board, i, j);


			//if square is already filled
				if(possibilities[9] == -1) {}

			//if a square has 0 possibilities
				else if (possibilities[9] == 0) {
					System.out.println("Board is not possible");
					System.exit(1);
				}

			//if a square has 1 possibility
				else if (possibilities[9] == 1) {
					int scan;
					for(scan = 0; scan < 9; scan++) {
						if(possibilities[scan] != 0) {
							board[i][j] = possibilities[scan];
							return board;
						}
					}
				}

			//square is blank but has > 1 possibility //catch all
				else {}
			}
		}

	//check in case the board is done
		if(boardIsSolved(board)) {
			return board;
		}

	//check in case nothing was updated
		else {
			System.out.println("Puzzle is not easy...");
			System.out.println("ATTEMPT TO SOLVE");
			printBoard(board);
			System.exit(1);
		}
	return board;
	}



/*
Method to check a spot on the board and return the possible number options
@param board is the Sudoku board we are working with
@param possibilities is a list of integers 1-9
	the last integer, possibilities[9] will be the number of possibilities for a spot
@param i is the column index of the spot
@param is the row index of the spot
@returns an array of possible integer values
	with the last integer being the number of possibilities
*/
	private static int[] spotCheck(int[][] board, int i, int j) {

		int o, k, scan;
		int[] possibilities = {1,2,3,4,5,6,7,8,9,0};

	//checking if spot is blank
		if(board[i][j] != 0) {
			possibilities[9] = -1;
			return possibilities;
		}

	//checking row
		for(o = 0; o < 9; o++) {

		//scanning through possibilities[] for matches
			for(scan = 0; scan < 9; scan++) {

			//removing possibility from list if there is a match
				if(board[o][j] == possibilities[scan]) {
					possibilities[scan] = 0;
				}
			}
		}

	//checking column
		for(k = 0; k < 9; k++) {

		//scanning through possibilities[] for matches
			for(scan = 0; scan < 9; scan++) {

			//removing possibility from list if there is a match
				if(board[i][k] == possibilities[scan]) {
					possibilities[scan] = 0;
				}
			}
		}

	//checking box
		for(o = ((i/3)*3); o < ((i/3)*3)+3; o++) {
			for(k = ((j/3)*3); k < ((j/3)*3)+3; k++) {

			//scanning through possibilities[] for matches
				for(scan = 0; scan < 9; scan++) {

				//removing possibility from list if there is a match
					if(board[o][k] == possibilities[scan]) {
						possibilities[scan] = 0;
					}
				}
			}
		}

	//counting number of possibilities
		int numPoss = 0;
		for(scan = 0; scan < 9; scan++) {
			if(possibilities[scan] != 0) {
				numPoss++;
			}
		}
		possibilities[9] = numPoss;
		return possibilities;
	}


/*
Method to check if a given game board is correct
@param board is the Sudoku game board to check
@returns true if solved and false if not
*/
	private static boolean boardIsSolved(int[][] board) {

	//looping through all elements in board
		for(int i = 0; i < 9; i++) {
			for(int j = 0; j < 9; j++) {

			//checking if number is 1-9
				if(board[i][j] < 1 
					|| board[i][j] > 9) {
					return false;
				}

			//checking row
				int o = ((i+1)%9);
				while(o != i) {

				//checking for duplicates
					if(board[o][j] == board[i][j]) {
						return false;
					}

				//updating index
					o = ((o+1)%9);
				}

			//checking column
				int k = ((j+1)%9);
				while(k != j) {

				//checking for duplicates
					if(board[i][k] == board[i][j]) {
						return false;
					}

				//updating index
					k = ((k+1)%9);
				}

			//checking box
				for(o = ((i/3)*3); o < (((i/3)*3)+3); o++) {
					for(k = ((j/3)*3); k < (((j/3)*3)+3); k++) {

					//checking for duplicates
						if(board[o][k] == board[i][j]
							&& o != i
							&& k != j) {
							return false;
						}
					}
				}
			} 
		}
		return true;
	}

/*
Method to print the original board and it's solved counterpart
@param origBoard is the unsolved board given
@param board is the solved board
*/
	private static void printBoard(int[][] board) {
	//looping through board
		for(int i = 0; i < 9; i++) {
			for(int j = 0; j < 9; j++) {
				System.out.print(board[i][j] + " ");
			}
			System.out.println();
		}
	}

/*
Method to write the solved board to an appropriately named file
@param board is the solved board
*/
	private static void writeBoard(int[][] board, String file) {
	//creating output file
		String outputFile = file.split("\\.")[0];
		outputFile += "_solution.txt";
		PrintWriter writer = null;

		try {
			writer = new PrintWriter(outputFile);
		}

		catch (FileNotFoundException e) {
			System.out.println("Could not create output file");
			System.exit(1);
		}

	//looping through board
		for(int i = 0; i < 9; i++) {
			for(int j = 0; j < 9; j++) {
				writer.print(board[i][j] + " ");
			}
			writer.println();
		}

	//closing PrintWriter stream
		writer.close();
	}
}
